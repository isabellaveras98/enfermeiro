package br.com.sudv.enfermeiro.controller;

import br.com.sudv.modelo.repositorio.EnfermeiroRepositorio;
import br.com.sudv.modelo.repositorio.VacinaRepositorio;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/enfermeiros")
public class EnfermeiroController {

    private EnfermeiroRepositorio enfermeiroRepositorio;
    private VacinaRepositorio vacinaRepositorioRepositorio;

    public EnfermeiroController(EnfermeiroRepositorio enfermeiroRepositorio, VacinaRepositorio vacinaRepositorioRepositorio) {
        this.enfermeiroRepositorio = enfermeiroRepositorio;
        this.vacinaRepositorioRepositorio = vacinaRepositorioRepositorio;
    }

    @GetMapping
    public ModelAndView getEnfermeirosTemplate() {
        ModelAndView mv = new ModelAndView();
        mv.addObject("enfermeiros", enfermeiroRepositorio.findByCoren("589"));
        mv.setViewName("enfermeiros");
        return mv;

    }

}
